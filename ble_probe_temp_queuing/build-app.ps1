﻿$adbPath = "C:\Program Files (x86)\GNU ARM Eclipse\Build Tools\bin\make.exe"

Function flash-setup-device
{
    Param()
    
    $installApp2 = $adbPath + " install -r .\Release\mqtt.apk"

    Invoke-Expression $installApp2
}

Function install-gateway-app
{
    Param()
    
    $installApp1 = $adbPath + " install -r .\Release\gateway.apk"

    Invoke-Expression $installApp1
}

Function delete-monitor-app
{
    Param()
    $step1 = $adbPath + " shell ""su -c 'mount -o rw,remount /system'"""
    $step2 = $adbPath + " shell ""su -c 'rm /system/app/gatewaymonitor.apk'"""
    #$step3 = $adbPath + " shell ""su -c 'pm uninstall com.tagbox.androidgatewaymonitor'"""

    Invoke-Expression $step1
    Invoke-Expression $step2
    #Invoke-Expression $step3
}

Function install-monitor-app
{
    Param()

    $pushMonitorApp = $adbPath + " push .\Release\gatewaymonitor.apk /sdcard/Download"
        
    $step4 = $adbPath + " shell ""su -c 'cp /storage/emulated/0/Download/gatewaymonitor.apk /system/app'"""
    $step5 = $adbPath + " shell ""su -c 'chmod 644 /system/app/gatewaymonitor.apk'"""

    Invoke-Expression $pushMonitorApp
    Invoke-Expression $step4
    Invoke-Expression $step5
}

Function reboot-gateway
{
    Param()
    
    $reboot = $adbPath + " reboot"
    
    Invoke-Expression $reboot
}

cmd.exe /c 'gradlew.bat clean'
cmd.exe /c 'gradlew.bat assembleRelease'
write-host "The Last Exit Code is:" $LastExitCode

if($LastExitCode -eq 0) {

    New-Item -ItemType Directory -Force -Path Release

    $gatewayApk = ".\app\build\outputs\apk"
    $monitoringApk = ".\androidgatewaymonitor\build\outputs\apk"
    $mqttApk = ".\mqttconnection\build\outputs\apk"

    $destpath = ".\Release"

    Remove-Item .\Release\*

    get-childitem -path $gatewayApk -Filter "Tagbox-Gateway-*.apk" |

        sort-object -Property LastWriteTime |
    
        select-object -last 1 | copy-item -Destination (join-path $destpath "gateway.apk")

    get-childitem -path $monitoringApk -Filter "Tagbox-GatewayMonitor-*.apk" |

        sort-object -Property LastWriteTime |

        select-object -last 1 | copy-item -Destination (join-path $destpath "gatewaymonitor.apk")

    get-childitem -path $mqttApk -Filter "Tagbox-Mqtt-*.apk" |

        sort-object -Property LastWriteTime |

        select-object -last 1 | copy-item -Destination (join-path $destpath "mqtt.apk")

    $listDevices = $adbPath + " devices"

    $result = Invoke-Expression $listDevices
    
    #Write-Host $result

    $devices = $result -replace "List of devices attached"
    
    Write-Host $devices

    if($devices -like '*device*') {
        install-gateway-app
        install-mqtt-app
        delete-monitor-app
        install-monitor-app
        reboot-gateway        
    }
}