#include <stdio.h>
#include <stdbool.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "SEGGER_RTT.h"
#include "sht31.h"

#define  NRF_LOG_MODULE_NAME 						"SHT31"
#include "nrf_log.h"

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{   

    switch(p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
			break;
        case NRF_DRV_TWI_EVT_ADDRESS_NACK:
            break;
        case NRF_DRV_TWI_EVT_DATA_NACK:
            break;
        default:
            break;        
    }   
}


/**
 * @brief twi initialization.
 */
uint32_t twi_init (nrf_drv_twi_t * p_twi)
{
    ret_code_t err_code;
    
    const nrf_drv_twi_config_t twi_config = {
       .scl                = DEVICE_SCL_PIN,
       .sda                = DEVICE_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };
    
    //err_code = nrf_drv_twi_init(p_twi, &twi_config, twi_handler, NULL);
    err_code = nrf_drv_twi_init(p_twi, &twi_config, NULL, NULL);
    if (err_code != NRF_SUCCESS)
    {
		NRF_LOG_ERROR("Err in I2C init: %d",err_code);
		return err_code;
	}
	return NRF_SUCCESS;
}

uint32_t crc8(const uint8_t *data, int len)				//changed by vipul
{

  const uint8_t POLYNOMIAL=0x31;
  uint8_t crc=0xFF;
  for ( int c = len; c; --c ) {
      crc ^= *data++;

      for ( int i = 8; i; --i ) {
			crc = ( crc & 0x80 )
			? (crc << 1) ^ POLYNOMIAL
			: (crc << 1);
      }
  }
  return crc;
}

/**
 * @brief Retrieve temp/humid data through i2c bus
 */
uint32_t get_temp_humid(nrf_drv_twi_t * p_twi, int16_t * p_temp, uint8_t * p_humid)
{
	nrf_drv_twi_enable(p_twi);

	uint8_t rx_data[6]={0};
	uint32_t err_code = NRF_SUCCESS;
	uint8_t config_sht[] ={0x2C,0x06};

	err_code = nrf_drv_twi_tx(p_twi, SLAVE_ADDRESS, config_sht, sizeof(config_sht), true);
	if (err_code != NRF_SUCCESS){
		NRF_LOG_ERROR("I2C tx err: %d\r\n",err_code);
		nrf_drv_twi_disable(p_twi);
		return err_code;
	}
		
	err_code = nrf_drv_twi_rx(p_twi, SLAVE_ADDRESS, rx_data, sizeof(rx_data));
	//APP_ERROR_CHECK(err_code);
	if (err_code != NRF_SUCCESS){
		NRF_LOG_ERROR("I2C rx err: %d\r\n",err_code);
		nrf_drv_twi_disable(p_twi);
		return err_code;
	}

	nrf_drv_twi_disable(p_twi);

	if (rx_data[2] != crc8(rx_data, 2)){ NRF_LOG_ERROR("crc ERROR");return SHT31_ERROR_INVALID_DATA;}    //changed by vipul
	if (rx_data[5] != crc8(rx_data+3, 2)){  NRF_LOG_ERROR("crc ERROR");return SHT31_ERROR_INVALID_DATA;} //changed by vipul

	*p_temp = (((rx_data[0] * 256) + rx_data[1])) / 3.74  - 4500;

	uint16_t humid_16b = (((rx_data[3] * 256) + rx_data[4])) / 655;
	*p_humid  = (uint8_t)humid_16b;
	return err_code;
}
