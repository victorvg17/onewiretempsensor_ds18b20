#include "nrf_gpio.h"
#include "nrf_delay.h"
#include <stdio.h>
#include <stdbool.h>
#include "nrf_log.h"
#include "main.h"

//pins requireed for one-wire
//#define ONEWIRE_PIN 10
#define ONEWIRE_PIN 30
#define VDD_ONEWIRE_PIN 27
#define GND_ONEWIRE_PIN 29

typedef struct
{
	int16_t			t_raw;					/*raw temperature value*/
	int16_t			t_dCel;					/*Temperature value in dCel*/

} dsb_processed_data_t;

/** Structure holding a 1-wire serial number. */

typedef struct sBSPACMonewireSerialNumber {
  /** The serial number in order MSB to LSB */
	uint8_t id[6];
} sBSPACMonewireSerialNumber;

#ifndef ONEWIRE_PIN
#define ONEWIRE_PIN 17
#endif /* ONEWIRE_PIN */


enum {
  /** Read 64-bit ROM code without using search procedure */
  BSPACM_ONEWIRE_CMD_READ_ROM = 0x33,

  /** Skip ROM sends the following command to all bus devices */
  BSPACM_ONEWIRE_CMD_SKIP_ROM = 0xCC,

  /** Determine whether device is parasite-powered or
   * external-powered */
  BSPACM_ONEWIRE_CMD_READ_POWER_SUPPLY = 0xB4,

  /** Store data from EEPROM into RAM */
  BSPACM_ONEWIRE_CMD_RECALL_EE = 0xB8,

  /** Read the RAM area. */
  BSPACM_ONEWIRE_CMD_READ_SCRATCHPAD = 0xBE,

  /** Initiate a temperature conversion.
   *
   * Be aware that temperature conversion can take up to 750ms at the
   * default 12-bit resolution.
   *
   * For an externally (non-parasite) powered sensor, the caller may
   * use #iBSPACMonewireReadBit_ni to determine whether the conversion
   * has completed.  Completion is indicated when the device responds
   * with a 1. */
  BSPACM_ONEWIRE_CMD_CONVERT_T = 0x44,
};

/** Define protocol state times in microseconds.
 *
 * @note Since all these times are far less than any sane watchdog
 * interval, and the timing can be important, BSPACM_CORE_DELAY_CYCLES
 * is not used in this module. */


/** Define protocol state times in microseconds.
 *
 * @note Since all these times are far less than any sane watchdog
 * interval, and the timing can be important, BSPACM_CORE_DELAY_CYCLES
 * is not used in this module. */
enum {
  /** Minimum time to hold bus low to ensure reset */
  OWT_RSTL_us = 480,

  /** Time to wait for presence detection after reset to quiesce */
  OWT_RSTH_us = 480,

  /** Delay before presence pulse is known to be valid (15us..60us) */
  OWT_PDHIGH_us = 60,

  /** Minimum time to hold bus low when writing a zero */
  OWT_LOW0_us = 60,

  /** Minimum time to hold bus low when writing a one */
  OWT_LOW1_us = 1,

  /** Recovery delay between write/read transaction cycles */
  OWT_REC_us = 1,

  /** Time to hold bus low when initiating a read slot */
  OWT_INT_us = 1,

  /** Point at which read value should be sampled */
  OWT_RDV_us = 15 - OWT_INT_us,

  /** Minimum duration of a read or write slot */
  OWT_SLOT_us = 60,
};

/*******************************Function declarations*******************************/
/*
uint8_t onewireReset_ni ();
void onewireShutdown_ni ();
void onewireWriteByte_ni (uint8_t byte);
uint8_t onewireReadBit_ni ();
uint8_t onewireReadByte_ni ();
int8_t onewireComputeCRC (const unsigned char * romp, uint8_t len);
int8_t onewireReadSerialNumber (sBSPACMonewireSerialNumber * snp);
int8_t onewireRequestTemperature_ni ();
int8_t onewireReadPowerSupply ();
*/
uint32_t onewire_init();
int8_t onewireReadTemperature_ni (int * temp_xCel);

