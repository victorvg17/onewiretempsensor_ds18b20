#include "ds18b20.h"

#define  NRF_LOG_MODULE_NAME 			"DSB"

/*
void dirclr ()
{
  NRF_GPIO->PIN_CNF[ONEWIRE_PIN] = 0
    | (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
    | (GPIO_PIN_CNF_DRIVE_S0S1     << GPIO_PIN_CNF_DRIVE_Pos)
    | (GPIO_PIN_CNF_PULL_Disabled  << GPIO_PIN_CNF_PULL_Pos)
    | (GPIO_PIN_CNF_INPUT_Connect  << GPIO_PIN_CNF_INPUT_Pos)
    | (GPIO_PIN_CNF_DIR_Input      << GPIO_PIN_CNF_DIR_Pos);
}
*/


void dirclr(){
nrf_gpio_cfg(
	ONEWIRE_PIN,
    NRF_GPIO_PIN_DIR_INPUT,
    NRF_GPIO_PIN_INPUT_CONNECT,
    NRF_GPIO_PIN_NOPULL,
    NRF_GPIO_PIN_S0S1,
    NRF_GPIO_PIN_NOSENSE);
}


uint8_t onewireReset_ni ()
{
  uint8_t present = 0;

  /* Non-standard: Hold bus high for OWT_RESET_us.  This provides
   * enough parasitic power for the device to signal presence.
   * Without this, effective RSTL duration may exceed the maximum
   * 960us before device reset. */
  nrf_gpio_cfg_output(ONEWIRE_PIN);
  nrf_gpio_pin_set(ONEWIRE_PIN);
  nrf_delay_us(OWT_RSTH_us);

  /* Hold bus low for OWT_RESET_us */
  nrf_gpio_pin_clear(ONEWIRE_PIN);
  nrf_delay_us(OWT_RSTL_us);

  /* Release bus and switch to input until presence pulse should be
   * visible. */
  dirclr();
  nrf_delay_us(OWT_PDHIGH_us);

  /* Record presence if bus is low (DS182x is holding it there) */
  nrf_gpio_cfg_input(ONEWIRE_PIN, NRF_GPIO_PIN_NOPULL);
  present = !(nrf_gpio_pin_read(ONEWIRE_PIN));

  /* Wait for reset cycle to complete */
  nrf_delay_us(OWT_RSTH_us - OWT_PDHIGH_us);

  return present;
}


void onewireShutdown_ni ()
{
  //NRF_GPIO->OUTCLR = ONEWIRE_BIT;
  dirclr();
}

void onewireWriteByte_ni (uint8_t byte)
{
  uint8_t bp;

  for (bp = 0; bp < 8; ++bp) {
	//@victor:
    //NRF_GPIO->OUTCLR = ONEWIRE_BIT;
    nrf_gpio_cfg_output(ONEWIRE_PIN);
    nrf_gpio_pin_clear(ONEWIRE_PIN);
    if (byte & 0x01) {
      nrf_delay_us(OWT_LOW1_us);
      dirclr();
      nrf_delay_us(OWT_SLOT_us - OWT_LOW1_us + OWT_REC_us);
    } else {
      nrf_delay_us(OWT_LOW0_us);
      dirclr();
      nrf_delay_us(OWT_SLOT_us - OWT_LOW0_us + OWT_REC_us);
    }
    byte >>= 1;
  }
}

uint8_t onewireReadBit_ni ()
{
  uint8_t rv;
  //NRF_GPIO->OUTCLR = ONEWIRE_BIT;
  nrf_gpio_cfg_output(ONEWIRE_PIN);
  nrf_gpio_pin_clear(ONEWIRE_PIN);

  //nrf_gpio_cfg_output(ONEWIRE_PIN);
  nrf_delay_us(OWT_INT_us);
  dirclr();
  nrf_delay_us(OWT_RDV_us);
  //vBSPACMledSet(0, 1);
  //@victor
  //rv = !!(NRF_GPIO->IN & ONEWIRE_BIT);
  nrf_gpio_cfg_input(ONEWIRE_PIN, NRF_GPIO_PIN_PULLUP);
  rv = nrf_gpio_pin_read(ONEWIRE_PIN);
  //vBSPACMledSet(0, 0);
  nrf_delay_us(OWT_SLOT_us - OWT_RDV_us - OWT_INT_us + OWT_REC_us);
  return rv;
}

uint8_t onewireReadByte_ni ()
{
  uint8_t byte = 0;
  uint16_t bit = 1;

  do {
    if (onewireReadBit_ni(nrf_delay_us)) {
      byte |= bit;
    }
    bit <<= 1;
  } while (0x100 != bit);
  return byte;
}

int8_t onewireComputeCRC (const unsigned char * romp,
                          uint8_t len)
{
  static const unsigned char OW_CRC_POLY = 0x8c;
  unsigned char crc = 0;

  while (0 < len--) {
    uint8_t bi;

    crc ^= *romp++;
    for (bi = 0; bi < 8; ++bi) {
      if (crc & 1) {
        crc = (crc >> 1) ^ OW_CRC_POLY;
      } else {
        crc >>= 1;
      }
    }
  }
  return crc;
}

int8_t onewireReadSerialNumber (sBSPACMonewireSerialNumber * snp)
{
  uint8_t rom[8];
  uint8_t i;
  int8_t rv = -1;

  do {
    if (! onewireReset_ni()) {
      break;
    }
    onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_READ_ROM);
    for (i = 0; i < sizeof(rom); ++i) {
      rom[i] = onewireReadByte_ni();
    }
    rv = 0;
  } while (0);

  if (0 == rv) {
    if (0 != onewireComputeCRC(rom, sizeof(rom))) {
      rv = -1;
    } else {
      for (i = 0; i < sizeof(snp->id); ++i) {
        snp->id[i] = rom[sizeof(rom) - 2 - i];
      }
    }
  }
  return rv;
}

int8_t onewireRequestTemperature_ni ()
{
  if (! onewireReset_ni()) {
    return -1;
  }
  onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_SKIP_ROM);
  onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_CONVERT_T);
  return 1;
}

int8_t onewireReadPowerSupply ()
{
  int rv = -1;

  if (onewireReset_ni()) {
    onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_SKIP_ROM);
    onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_READ_POWER_SUPPLY);
    rv = onewireReadBit_ni();
  }
  return rv;
}

int8_t onewireReadTemperature_ni (int * temp_xCel)
{
  int t;

  if (! onewireReset_ni()) {
    return -1;
  }
  onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_SKIP_ROM);
  onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_READ_SCRATCHPAD);
  t = onewireReadByte_ni();
  t |= (onewireReadByte_ni() << 8);
  *temp_xCel = t;
  return 0;
}

int8_t crc_scratchpaddata(const unsigned char * romp, uint8_t len)
{
	uint8_t scr_pad[8];
	onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_SKIP_ROM);
	onewireWriteByte_ni(BSPACM_ONEWIRE_CMD_READ_SCRATCHPAD);
    for (uint8_t i = 0; i < sizeof(scr_pad); ++i) {
      scr_pad[i] = onewireReadByte_ni();
    }
    int8_t crc = 0;
    crc = onewireComputeCRC(scr_pad, sizeof(scr_pad));

  }

uint32_t onewire_init()
{
	uint8_t err_code = 0;
	//@victor : set pin 27 as VDD for one-wire
	nrf_gpio_cfg_output(VDD_ONEWIRE_PIN);
	nrf_gpio_pin_set(VDD_ONEWIRE_PIN);

	//@victor : set pin 29 as GND for one-wire
	nrf_gpio_cfg_output(GND_ONEWIRE_PIN);
	nrf_gpio_pin_clear(GND_ONEWIRE_PIN);


	err_code = onewireReset_ni();
	if (!err_code) NRF_LOG_INFO("ERR: No DS18B20 present on P0.%u since onewireReset_ni(): %d\n", ONEWIRE_PIN, onewireReset_ni());

	//@victor : verify the power source for sensor (external since there is seperate VDD line to power up sensor
	static const char * const supply_type[] = { "parasitic", "external" };
	int8_t external_power = onewireReadPowerSupply();
	NRF_LOG_INFO("Power supply: %s\n", supply_type[external_power]);
	if (0 > external_power) NRF_LOG_INFO("ERROR: Device not present?\n");

	//@victor : read the seial number of slave on the 1-wire bus.
	sBSPACMonewireSerialNumber serial;
	int rc = onewireReadSerialNumber(&serial);
	NRF_LOG_INFO("Serial got %d: \n", rc);
	NRF_LOG_INFO("Serial id : \n");
	for(uint8_t itr = 0;itr < 6; itr++) {
		NRF_LOG_INFO("%02x \n",serial.id[itr]);
	}

	return err_code;
}
