#include <stdio.h>
#include <stdbool.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "SEGGER_RTT.h"
#include "sht20.h"

#define  NRF_LOG_MODULE_NAME 						"SHT31"
#include "nrf_log.h"

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{   

    switch(p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
			break;
        case NRF_DRV_TWI_EVT_ADDRESS_NACK:
            break;
        case NRF_DRV_TWI_EVT_DATA_NACK:
            break;
        default:
            break;        
    }   
}


/**
 * @brief twi initialization.
 */
uint32_t twi_init (nrf_drv_twi_t * p_twi)
{
    ret_code_t err_code;
    
    const nrf_drv_twi_config_t twi_config = {
       .scl                = DEVICE_SCL_PIN,
       .sda                = DEVICE_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };
    
    //err_code = nrf_drv_twi_init(p_twi, &twi_config, twi_handler, NULL);
    err_code = nrf_drv_twi_init(p_twi, &twi_config, NULL, NULL);
    if (err_code != NRF_SUCCESS)
    {
		NRF_LOG_ERROR("Err in I2C init: %d",err_code);
		return err_code;
	}
	return NRF_SUCCESS;
}

uint32_t crc8(const uint8_t *data, int len)				//changed by vipul
{

  const uint8_t POLYNOMIAL=0x31;
  uint8_t crc=0xFF;
  for ( int c = len; c; --c ) {
      crc ^= *data++;

      for ( int i = 8; i; --i ) {
			crc = ( crc & 0x80 )
			? (crc << 1) ^ POLYNOMIAL
			: (crc << 1);
      }
  }
  return crc;
}

uint32_t sht20_get_temp(nrf_drv_twi_t * p_twi, int16_t *temperature)
{
	uint8_t rx_data[2]={0,0};
  uint8_t reg = SHT20_TRIGGER_TEMPERATURE_MEAS;

	nrf_drv_twi_enable(p_twi);
	
	uint32_t err_code = nrf_drv_twi_tx(p_twi, SLAVE_ADDRESS, &reg, 1, true);
	if (err_code != NRF_SUCCESS) return err_code;
	nrf_delay_ms(SHT20_MEAS_TIME_TEMPERATURE);

	err_code = nrf_drv_twi_rx(p_twi, SLAVE_ADDRESS, rx_data, sizeof(rx_data));

	nrf_drv_twi_disable(p_twi);

	if ((*temperature & SHT20_RESULT_TYPE) != SHT20_TEMPERATURE_RESULT) {
		return SHT20_ERROR_INVALID_DATA;	/* we requested a temperature measurement */
	}
	
	*temperature = (rx_data[0]<<8)&rx_data[0];
	*temperature = *temperature & ~SHT20_STATUS_BITS_MASK;	/* zero out status bits */
	*temperature = -4685 + ((17572 * *temperature) >> 16);

	return err_code;
}
