#include <stdio.h>
#include <stdbool.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "SEGGER_RTT.h"
#include "kx022.h"
#define  NRF_LOG_MODULE_NAME 						"KX022"
#include "nrf_log.h"
#include "math.h"

volatile bool evtDone = false;


void reinit_i2c(nrf_drv_twi_t * p_twi)
{
    const nrf_drv_twi_config_t twi_config = {
       .scl                = DEVICE_SCL_PIN,
       .sda                = DEVICE_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };

	nrf_drv_twi_disable(p_twi);
	nrf_drv_twi_init(p_twi, &twi_config, NULL, NULL);

	nrf_drv_twi_enable(p_twi);

}

/**
 * @brief TWI events handler.
 */
void kx022_twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
	NRF_LOG_DEBUG("In kx022 twi handler");
    switch(p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
        		NRF_LOG_DEBUG("i2c event done");
        		evtDone = true;
			break;
        case NRF_DRV_TWI_EVT_ADDRESS_NACK:
    			NRF_LOG_DEBUG("i2c address nack");
        	break;
        case NRF_DRV_TWI_EVT_DATA_NACK:
        		NRF_LOG_DEBUG("i2c data nack");
        	break;
        default:
            break;
    }
}


/**
 * @brief twi initialization.
 */
uint32_t kx022_init (nrf_drv_twi_t * p_twi)
{
    uint32_t err_code;
    uint8_t reg[2];
    
    const nrf_drv_twi_config_t twi_config = {
       .scl                = DEVICE_SCL_PIN,
       .sda                = DEVICE_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };

    //err_code = nrf_drv_twi_init(p_twi, &twi_config, kx022_twi_handler, NULL);
    err_code = nrf_drv_twi_init(p_twi, &twi_config, NULL, NULL);
    if (err_code != NRF_SUCCESS)
    {
		NRF_LOG_ERROR("Err in I2C init: %d",err_code);
		return err_code;
	}

    // Set nCS pin to 1 to enable I2C communication
    nrf_gpio_cfg_output(CHIPSEL_B_PIN);
    nrf_gpio_pin_write(CHIPSEL_B_PIN,1);

    // Set ADDR pin to 0
    nrf_gpio_cfg_output(ADDR_PIN);
    nrf_gpio_pin_write(ADDR_PIN, 1);

    NRF_LOG_INFO("KX022 init started\n\r");

	nrf_drv_twi_enable(p_twi);

    reg[0] = KX022_CNTL1;
    reg[1] = 0x10;
	err_code = nrf_drv_twi_tx(p_twi, KX022_ADDRESS, &reg[0], 2, true);
    if (err_code !=NRF_SUCCESS) NRF_LOG_ERROR("KX022 CNTL1 reg write error : %d",err_code); 
    
    reg[0] = KX022_ODCNTL;
    reg[1] = 0x09;					// Output data rate set at 0x09 = 1.563Hz
	err_code = nrf_drv_twi_tx(p_twi, KX022_ADDRESS, reg, sizeof(reg),false);
    if (err_code !=NRF_SUCCESS) NRF_LOG_ERROR("KX022 ODCNTL reg write error : %d",err_code); 

    reg[0] = KX022_CNTL3;
    reg[1] = 0xD8;
	err_code = nrf_drv_twi_tx(p_twi, KX022_ADDRESS, reg, sizeof(reg),false);
    if (err_code !=NRF_SUCCESS) NRF_LOG_ERROR("KX022 CNTL3 reg write error : %d",err_code);

    reg[0] = KX022_TILT_TIMER;
    reg[1] = 0x01;
	err_code = nrf_drv_twi_tx(p_twi, KX022_ADDRESS, reg, sizeof(reg),false);
    if (err_code !=NRF_SUCCESS) NRF_LOG_ERROR("KX022 TILT_TIMER reg write error : %d",err_code);
    
    reg[0] = KX022_CNTL1;
    reg[1] = 0x90;
	err_code = nrf_drv_twi_tx(p_twi, KX022_ADDRESS, reg, sizeof(reg),false);
    if (err_code !=NRF_SUCCESS) NRF_LOG_ERROR("KX022 CNTL1 reg write error : %d",err_code);

    NRF_LOG_INFO("KX022 init done");
	nrf_drv_twi_disable(p_twi);

    return NRF_SUCCESS;
}

static uint32_t get_acc_onAxis(nrf_drv_twi_t * p_twi, uint8_t addr, int16_t *acc)
{
	uint8_t rx_data[2];
	uint8_t reg_addr[1] = {addr};
	uint32_t err_code;
	
	err_code = nrf_drv_twi_tx(p_twi, KX022_ADDRESS, reg_addr, sizeof(reg_addr),true);
    if (err_code !=NRF_SUCCESS) 
	{
		NRF_LOG_ERROR("Read start error on reg %02x : %d\r\n", reg_addr[0], err_code);
		NRF_LOG_WARNING("Resetting device\r\n");
		//reinit_i2c(p_twi);
		//sd_nvic_SystemReset();
		return err_code;
	}
    
	err_code = nrf_drv_twi_rx(p_twi, KX022_ADDRESS, rx_data, sizeof(rx_data));
    if (err_code !=NRF_SUCCESS) return err_code;
    
	*acc = ((rx_data[1] << 8) | rx_data[0]);
	return NRF_SUCCESS;
}


/**
 * @brief Retrieve temp/humid data through i2c bus
*/
uint32_t get_acc_3axes_16b(nrf_drv_twi_t * p_twi, int16_t * accArray)
{
	uint32_t err_code = NRF_SUCCESS;
	nrf_drv_twi_enable(p_twi);
	//uint8_t acc_byteArray[6] = {0,0,0,0,0,0};			// {x_h,x_l,y_h,y_l,z_h,z_l}

	err_code = get_acc_onAxis(p_twi, KX022_XOUT_L, accArray);
	if (err_code != NRF_SUCCESS) { NRF_LOG_ERROR("accX err:%d",err_code); nrf_drv_twi_disable(p_twi); return err_code;}
	err_code = get_acc_onAxis(p_twi, KX022_YOUT_L, (accArray+1));
	if (err_code != NRF_SUCCESS) { NRF_LOG_ERROR("accY err:%d",err_code); nrf_drv_twi_disable(p_twi); return err_code;}
	err_code = get_acc_onAxis(p_twi, KX022_ZOUT_L, (accArray+2));
	if (err_code != NRF_SUCCESS) { NRF_LOG_ERROR("accZ err:%d",err_code); nrf_drv_twi_disable(p_twi); return err_code;}

	nrf_drv_twi_disable(p_twi);
	return NRF_SUCCESS;
}

uint32_t get_acc_vectorsum(nrf_drv_twi_t * p_twi, uint16_t * output)
{
	int16_t acc_array[] = {0,0,0};
	uint32_t err_code = get_acc_3axes_16b(p_twi, &acc_array[0]);
	if (err_code != NRF_SUCCESS) return err_code;

	int16_t accX = acc_array[0];
	int16_t accY = acc_array[1];
	int16_t accZ = acc_array[2];

	*output = sqrt(accX*accX + accY*accY + accZ*accZ);

	NRF_LOG_DEBUG("X:%d, Y:%d, Z:%d, VSUM:%d\n", accX, accY, accZ, *output);
	return NRF_SUCCESS;
}
