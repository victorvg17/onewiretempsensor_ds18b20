#define  NRF_LOG_MODULE_NAME 						"BATTERY_LEVEL"
#include "nrf_log.h"
#include "battery_level.h"
#include "bledb.h"

volatile int32_t offsetError, gainError = 0;


#ifdef ADC_PRESENT
static nrf_adc_value_t adc_buf[1];
#else
static nrf_saadc_value_t adc_buf[2];
#endif //ADC_PRESENT


/** @brief Function for converting the input voltage (in milli volts) into percentage of 3.0 Volts.
 *
 *  @details The calculation is based on a linearized version of the battery's discharge
 *           curve. 3.0V returns 100% battery level. The limit for power failure is 2.1V and
 *           is considered to be the lower boundary.
 *
 *           The discharge curve for CR2032 is non-linear. In this model it is split into
 *           4 linear sections:
 *           - Section 1: 3.0V - 2.9V = 100% - 42% (58% drop on 100 mV)
 *           - Section 2: 2.9V - 2.74V = 42% - 18% (24% drop on 160 mV)
 *           - Section 3: 2.74V - 2.44V = 18% - 6% (12% drop on 300 mV)
 *           - Section 4: 2.44V - 2.1V = 6% - 0% (6% drop on 340 mV)
 *
 *           These numbers are by no means accurate. Temperature and
 *           load in the actual application is not accounted for!
 *
 *  @param[in] mvolts The voltage in mV
 *
 *  @return    Battery level in percent.
*/
static uint8_t battery_level_in_percent_fr03(uint16_t mvolts)
{
    uint8_t battery_level=0;

    if (mvolts >= 3400)
	 {
		 battery_level = 100;
	 }
	 else if (mvolts> 3300)
	 {
		 battery_level = (46923 * mvolts)/1000/100- 1469;
	 }
	 else if (mvolts> 3200)
	 {
		 battery_level = (769* mvolts)/1000/100 +28;
	 }
	 else if (mvolts> 2900)
	 {
		 battery_level = (15384* mvolts)/1000/100 - 431;
	 }
	 else if (mvolts> 2800)
	 {
		 battery_level = (7692 * mvolts)/1000/100 - 215;
	 }
	 else
	 {
		 battery_level = 0;
	 }
    return battery_level;
}

/** @brief Function for converting the input voltage (in milli volts) into percentage of 3.0 Volts.
 *
 *  @details Real battery profile needs to be verified again Panasonic batteries
 *  @param[in] mvolts The voltage in mV
 *
 *  @return    Battery level in percent.
*/
static uint8_t battery_level_in_percent_cr2477(uint16_t mvolts)
{
    uint8_t battery_level=0;

    if (mvolts >= 3000)
	{
		battery_level = 100;
	}
	else if (mvolts> 2800)
	{
		battery_level = (3125* mvolts )/1000/100 + 6;
	}
	else if (mvolts> 2600)
	{
		battery_level = (427* mvolts)/1000 - 1102;
	}
	else if (mvolts> 2500)
	{
		battery_level = (83*mvolts)/1000 - 208;
	}
	else
	{
		battery_level = 0;
	}
    return battery_level;
}



/** @brief Function for converting the input voltage (in milli volts) into percentage of 3.0 Volts.
 *
 *  @details Real battery profile needs to be verified again Panasonic batteries
 *  @param[in] mvolts The voltage in mV
 *
 *  @return    Battery level in percent.
*/
static uint8_t battery_level_in_percent_er14250(uint16_t mvolts)
{
    uint8_t battery_level=0;

    if (mvolts >= 3600)
	{
		battery_level = 100;
	}
	else if (mvolts> 3550)
	{
		//battery_level = (1200* mvolts )/1000 + -4200;
		battery_level = (200* mvolts)/1000 - 670;
	}
	else if (mvolts> 3500)
	{
		battery_level = (200* mvolts)/1000 - 670;
	}
	else if (mvolts> 3450)
	{
		battery_level = (200* mvolts)/1000 - 670;
	}
	else if (mvolts> 3400)
	{
		battery_level = (200* mvolts)/1000 - 670;
	}
	else
	{
		battery_level = 0;
	}
    return battery_level;
}

/** @brief Function for converting the input voltage (in milli volts) into percentage of 3.0 Volts.
 *
 *  @details The calculation is based on a linearized version of the battery's discharge
 *           curve. 3.0V returns 100% battery level. The limit for power failure is 2.1V and
 *           is considered to be the lower boundary.
 *
 *           The discharge curve for CR2032 is non-linear. In this model it is split into
 *           4 linear sections:
 *           - Section 1: 3.0V - 2.9V = 100% - 42% (58% drop on 100 mV)
 *           - Section 2: 2.9V - 2.74V = 42% - 18% (24% drop on 160 mV)
 *           - Section 3: 2.74V - 2.44V = 18% - 6% (12% drop on 300 mV)
 *           - Section 4: 2.44V - 2.1V = 6% - 0% (6% drop on 340 mV)
 *
 *           These numbers are by no means accurate. Temperature and
 *           load in the actual application is not accounted for!
 *
 *  @param[in] mvolts The voltage in mV
 *
 *  @return    Battery level in percent.
*/
static uint8_t battery_level_in_percent_cr2032(uint16_t mvolts)
{
    uint8_t battery_level;

    if (mvolts >= 3200)
	 {
		 battery_level = 100;
	 }
	else if (mvolts> 2950)
	{
		battery_level = (1090* mvolts)/1000/100 + 65;
	}
	else if (mvolts> 2900)
	{
		battery_level = (1218* mvolts)/1000 - 3496;
	}
	else if (mvolts> 2600)
	{
		battery_level = (121* mvolts)/1000 - 315;
	}
	else
	{
		battery_level = 0;
	}
    return battery_level;
}

#ifdef ADC_PRESENT
/**@brief Function for handling the ADC driver eevent.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
void adc_event_handler(nrf_drv_adc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_ADC_EVT_DONE)
    {
        nrf_adc_value_t adc_result;
        uint16_t        batt_lvl_in_milli_volts;
        uint8_t         percentage_batt_lvl;
        uint32_t        err_code;

        adc_result = p_event->data.done.p_buffer[0];

        err_code = nrf_drv_adc_buffer_convert(p_event->data.done.p_buffer, 1);
    

        batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(adc_result) +
                                  DIODE_FWD_VOLT_DROP_MILLIVOLTS;
        percentage_batt_lvl = battery_level_in_percent_fr03(batt_lvl_in_milli_volts);

				if (err_code != NRF_SUCCESS)
				{
					NRF_LOG_ERROR("Battery level conversion failed\r\n");
				}
				else
				{
					NRF_LOG_DEBUG("Battery level (from callback): %d\r\n",percentage_batt_lvl);
				}
    }
}


#else // SAADC_PRESENT
/**@brief Function for handling the ADC interrupt.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
void saadc_event_handler(nrf_drv_saadc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
		nrf_saadc_value_t adc_result;
		uint16_t          batt_lvl_in_milli_volts;
		uint8_t           percentage_batt_lvl;
		uint32_t          err_code;

		adc_result = p_event->data.done.p_buffer[0];

		err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, 1);

		if (err_code != NRF_SUCCESS)
		{
			NRF_LOG_ERROR("Battery level conversion failed\r\n");
		}
		else
		{
			NRF_LOG_DEBUG("Battery level (from callback): %d\r\n",percentage_batt_lvl);
		}
    }
}


#endif // ADC_PRESENT


/**@brief Function for configuring ADC to do battery level conversion.
 */
void adc_configure(void)
{
    #ifdef ADC_PRESENT
    ret_code_t err_code = nrf_drv_adc_init(NULL, adc_event_handler);

    static nrf_drv_adc_channel_t channel =
    NRF_DRV_ADC_DEFAULT_CHANNEL(NRF_ADC_CONFIG_INPUT_DISABLED);
    // channel.config.config.input = NRF_ADC_CONFIG_SCALING_SUPPLY_ONE_THIRD;
    channel.config.config.input = (uint32_t)NRF_ADC_CONFIG_SCALING_SUPPLY_ONE_THIRD;
    nrf_drv_adc_channel_enable(&channel);
    err_code = nrf_drv_adc_buffer_convert(&adc_buf[0], 1);

    #else //  SAADC_PRESENT
    ret_code_t err_code = nrf_drv_saadc_init(NULL, saadc_event_handler);
    APP_ERROR_CHECK(err_code);
    nrf_saadc_channel_config_t config =  NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_VDD);
    err_code = nrf_drv_saadc_channel_init(0, &config);
    APP_ERROR_CHECK(err_code);
//    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[0], 1);
//    APP_ERROR_CHECK(err_code);
//    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[1], 1);
//   APP_ERROR_CHECK(err_code);
    #endif //ADC_PRESENT
}

uint8_t get_battery_level(void)
{
		uint8_t percentage_batt_lvl = 0;

#ifdef NRF51
		percentage_batt_lvl = 100;
		return percentage_batt_lvl;
#else
		uint16_t batt_lvl_in_milli_volts;
		uint32_t err_code;
		nrf_saadc_value_t	batt_soc;
		err_code = nrf_drv_saadc_sample_convert(0,&batt_soc);

		if (err_code != NRF_SUCCESS)
		{
			percentage_batt_lvl = ADC_CONVERT_ERROR;
			NRF_LOG_ERROR("ADC single conv error : %d", err_code);
		}
		else
		{
# ifdef BB_DEVICE
			batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(batt_soc);
			//percentage_batt_lvl = battery_level_in_percent_cr2032(batt_lvl_in_milli_volts);
			/*DSB probe is using battery CR2477 battery*/
			//percentage_batt_lvl = battery_level_in_percent_cr2477(batt_lvl_in_milli_volts);
			percentage_batt_lvl = battery_level_in_percent_er14250(batt_lvl_in_milli_volts);
# else
#  ifdef SH_DEVICE
			batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(batt_soc);
			percentage_batt_lvl = battery_level_in_percent_cr2477(batt_lvl_in_milli_volts);
#  endif
#  ifdef XT_DEVICE
			# ifdef BI_DEVICE
				batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(batt_soc);
				percentage_batt_lvl = battery_level_in_percent_cr2477(batt_lvl_in_milli_volts);
			# else
				batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(batt_soc);
				percentage_batt_lvl = battery_level_in_percent_fr03(batt_lvl_in_milli_volts);
			# endif
#  endif
# endif
		}
		return percentage_batt_lvl;
#endif
}

void internal_temp_calib(calib_error_type_t calibType, int32_t calibValue)
{
	switch(calibType)
	{
		case TEMP_CALIB_OFFSET:
			offsetError = calibValue;
			break;
		case TEMP_CALIB_GAIN:
			gainError = calibValue;
			break;
		default:
			break;
	}

    NRF_LOG_INFO("Internal temperature offset = %d/100 degreeC, gain = %d/100 percent\r\n", offsetError,gainError);
}

/**@brief Function to get internal temperature of IC. Used for calibration */
uint32_t get_internal_temp(void)
{
    int32_t temp;

    sd_temp_get(&temp);

    temp = (temp*25 - offsetError)*10000/(10000 + gainError); // linear calibration from empirical data;
    int8_t exponent = -2;
    return ((exponent & 0xFF) << 24) | (temp & 0x00FFFFFF);
}


