#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "SEGGER_RTT.h"


// KX022's pins going to nRF
#define DEVICE_SCL_PIN 		6
#define DEVICE_SDA_PIN 		4
#define CHIPSEL_B_PIN		7
#define ADDR_PIN			5

#define KX022_ADDRESS			0x1F
#define KX022_WAI_VAL         (0x14)

#define KX022_XOUT_L          (0x06)
#define KX022_XOUT_H          (0x07)
#define KX022_YOUT_L          (0x08)
#define KX022_YOUT_H          (0x09)
#define KX022_ZOUT_L          (0x0A)
#define KX022_ZOUT_H          (0x0B)
#define KX022_WHO_AM_I        (0x0F)
#define KX022_CNTL1           (0x18)
#define KX022_CNTL3           (0x1A)
#define KX022_ODCNTL          (0x1B)
#define KX022_TILT_TIMER      (0x22)


uint32_t kx022_init (nrf_drv_twi_t * p_twi);

uint32_t get_acc_3axes_8b(nrf_drv_twi_t * p_twi, int8_t * accArray);

uint32_t get_acc_3axes_16b(nrf_drv_twi_t * p_twi, int16_t * accArray);

uint32_t get_acc_vectorsum(nrf_drv_twi_t * p_twi, uint16_t * output);
