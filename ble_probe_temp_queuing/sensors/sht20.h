#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "SEGGER_RTT.h"


// SHT31's SCL AND SDA PINS on nRF"
#define DEVICE_SCL_PIN 		24
#define DEVICE_SDA_PIN 		25
#define SLAVE_ADDRESS 		0x44
#define SHT20_TRIGGER_TEMPERATURE_MEAS 0xF3
#define SHT20_MEAS_TIME_TEMPERATURE			85
#define SHT20_MEAS_TIME_HUMIDITY				29
#define SHT20_RESULT_TYPE			0x02
#define SHT20_TEMPERATURE_RESULT		0x00
#define SHT20_HUMIDITY_RESULT	0x02
#define SHT20_STATUS_BITS_MASK 0x03

#define SHT20_ERROR_INVALID_DATA	0xFFFFFFFF

void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context);

uint32_t twi_init (nrf_drv_twi_t * p_twi);

uint32_t sht20_get_temp(nrf_drv_twi_t * p_twi, int16_t *temperature);