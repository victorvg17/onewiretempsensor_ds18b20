#include <stdint.h>
#include <string.h>
#include "nrf_gpio.h"
#include "ble_tbs.h"
#include "ble_srv_common.h"
#include "app_error.h"
#include "bledb.h"
#include "battery_level.h"

#define  NRF_LOG_MODULE_NAME 						"BLE_TBS"
#include "nrf_log.h"
#include "main.h"


// Declaration of a function for housekeeping of ble connections related to tbs service and characteristic
void ble_tbs_on_ble_evt(ble_tbs_t * p_tbs, ble_evt_t * p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
	{
		case BLE_GAP_EVT_CONNECTED:
			p_tbs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
			break;
		case BLE_GAP_EVT_DISCONNECTED:
			p_tbs->conn_handle = BLE_CONN_HANDLE_INVALID;
			break;
		case BLE_GATTS_EVT_WRITE:
		{
			uint8_t * pdata = p_ble_evt->evt.gatts_evt.params.write.data;
			uint16_t len = p_ble_evt->evt.gatts_evt.params.write.len;
			ble_uuid_t uuid = p_ble_evt->evt.gatts_evt.params.write.uuid;
			uint32_t err_code = FDS_SUCCESS;

			// Handle FDS event queue being full, i.e. FDS_ERROR_BUSY (8)
			// Implement calib function or tx_power change only after the write has been done

			switch (uuid.uuid)
			{
				case BLE_UUID_TBS_CHAR_INTERNALTEMP_GAINERR:
					;
					int32_t gainError = (int32_t)(int16_t)((*(pdata+1))<<8)|(*pdata);
					fds_find_and_delete(BACKUP_FILE_ID, REC_KEY_GAINERR);
					fds_write(BACKUP_FILE_ID, REC_KEY_GAINERR, &gainError, 1);
					wait_for_fds_evt(FDS_EVT_WRITE);
					internal_temp_calib(TEMP_CALIB_GAIN, gainError);
					NRF_LOG_INFO("Gain error value updated to %08x\r\n", gainError);
					break;
				case BLE_UUID_TBS_CHAR_INTERNALTEMP_OFFERR:
					;
					int32_t offsetError = (int32_t)(int16_t)((*(pdata+1))<<8)|(*pdata);
					fds_find_and_delete(BACKUP_FILE_ID, REC_KEY_OFFERR);
					fds_write(BACKUP_FILE_ID, REC_KEY_OFFERR, &offsetError, 1);
					wait_for_fds_evt(FDS_EVT_WRITE);
					internal_temp_calib(TEMP_CALIB_OFFSET, offsetError);
					NRF_LOG_INFO("Offset error value updated to %08x\r\n", offsetError);
					break;
				case BLE_UUID_TBS_CHAR_TXPOWER:
					;
					int32_t txPower = (int32_t)(int8_t)*pdata;
					if ((txPower == 4)||(txPower == 0)||(txPower == -10)||(txPower == -20)||(txPower == -30))
					{
						fds_find_and_delete(BACKUP_FILE_ID, REC_KEY_TXPOWER);
						fds_write(BACKUP_FILE_ID, REC_KEY_TXPOWER, &txPower, 1);
						sd_ble_gap_tx_power_set(txPower);
						NRF_LOG_INFO("Tx Power updated to %04x\r\n", txPower);
					}
					else NRF_LOG_WARNING("Invalid TX Power value entered");
					break;
				default:
					break;

			}
		}
			break;
		default:
			// No implementation needed.
			break;
	}
}

/**@brief Function for adding our new characterstic to TagBox Service 
 *
 * @param[in]   p_tbs        tagbox service structure.
 *
 */
static uint32_t tbs_char_add(ble_tbs_t * p_tbs,
				uint16_t char_uuid16,
				uint8_t * p_char_value, uint8_t char_value_len,
				uint8_t isWritable)
{
    // Add a custom characteristic UUID
	uint32_t            err_code;
	ble_uuid_t          char_uuid;
	ble_uuid128_t       base_uuid = BLE_UUID_TBS_BASE;
	char_uuid.uuid      = char_uuid16;
	uint8_t i;
	uint8_t value[char_value_len];

	err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
	if (err_code != NRF_SUCCESS) return err_code;

	// Set up properties to characteristic
    ble_gatts_char_md_t char_md;
    memset(&char_md, 0, sizeof(char_md));
	char_md.char_props.read = 1;
	char_md.char_props.write = isWritable;

    
    // Configure Client Characteristic Configuration Descriptor metadata and add to char_md structure
    //ble_gatts_attr_md_t cccd_md;
    //memset(&cccd_md, 0, sizeof(cccd_md));
   
    
    //  Configure the attribute metadata
    ble_gatts_attr_md_t attr_md;
    memset(&attr_md, 0, sizeof(attr_md));  
    attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    
    // Set read/write security levels to characteristic
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    //  Configure the characteristic value attribute
    ble_gatts_attr_t    attr_char_value;
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid      = &char_uuid;
	attr_char_value.p_attr_md   = &attr_md;

    // Set characteristic length in number of bytes
	attr_char_value.max_len     	= char_value_len;
	attr_char_value.init_len    	= char_value_len;

	for (i=0;i<char_value_len;i++)
	{
		value[i]		 			= *(p_char_value+i);
	}
	attr_char_value.p_value     	= value;

    // Add new characteristic to the service
	err_code = sd_ble_gatts_characteristic_add(p_tbs->service_handle,
							   &char_md,
							   &attr_char_value,
							   &p_tbs->char_handles);
	if (err_code != NRF_SUCCESS) return err_code;

    return NRF_SUCCESS;
}


/**@brief Function for initiating our new service.
 *
 * @param[in]   p_our_service        Our Service structure.
 *
 */
uint32_t ble_tbs_init(ble_tbs_t * p_tbs)
{
    uint32_t   err_code; // Variable to hold return codes from library and softdevice functions

    // FROM_SERVICE_TUTORIAL: Declare 16-bit service and 128-bit base UUIDs and add them to the BLE stack
    ble_uuid_t        service_uuid;
    ble_uuid128_t     base_uuid = BLE_UUID_TBS_BASE;
    service_uuid.uuid = BLE_UUID_TBS;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    if (err_code != NRF_SUCCESS) return err_code;

	
    // Set tbs service connection handle to default value, i.e. an invalid handle at system startup
		p_tbs->conn_handle = BLE_CONN_HANDLE_INVALID;
	
    // Add tbs service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &service_uuid,
                                        &p_tbs->service_handle);
    
    if (err_code != NRF_SUCCESS) return err_code;

    // add new characteristics to the service and initialize them for previously stored values from flash
    // also initialize the values in their respective scopes where required (eg gain/offset in internal temp)
	uint32_t init_value[1] = {0};
	uint8_t len_init_value = 1;
	fds_read(BACKUP_FILE_ID, REC_KEY_GAINERR, init_value, &len_init_value);
	uint8_t *p_gainError = (uint8_t *)init_value;
	err_code = tbs_char_add(p_tbs, BLE_UUID_TBS_CHAR_INTERNALTEMP_GAINERR, p_gainError, 2, 1);
    if (err_code != NRF_SUCCESS) return err_code;
    else internal_temp_calib(TEMP_CALIB_GAIN, (int32_t)init_value[0]);

	fds_read(BACKUP_FILE_ID, REC_KEY_OFFERR, init_value, &len_init_value);
	uint8_t * p_offsetError = (uint8_t *)init_value;
	err_code = tbs_char_add(p_tbs, BLE_UUID_TBS_CHAR_INTERNALTEMP_OFFERR, p_offsetError, 2, 1);
    if (err_code != NRF_SUCCESS) return err_code;
    else internal_temp_calib(TEMP_CALIB_OFFSET, (int32_t)init_value[0]);

	fds_read(BACKUP_FILE_ID, REC_KEY_TXPOWER, init_value, &len_init_value);
	uint8_t * p_txPower = (uint8_t *)init_value;
	err_code = tbs_char_add(p_tbs, BLE_UUID_TBS_CHAR_TXPOWER, p_txPower, 1, 1);
	if (err_code != NRF_SUCCESS) return NRF_SUCCESS;

	// Read only Characteristics
	// Logging interval in seconds
	uint8_t logInterval[] = {((ADV_INTERVAL_IN_MS*LOGINTERVAL_ADVINTERVAL_RATIO)/1000)/256,
								((ADV_INTERVAL_IN_MS*LOGINTERVAL_ADVINTERVAL_RATIO)/1000) % 256};
	err_code = tbs_char_add(p_tbs, BLE_UUID_TBS_CHAR_LOGINTERVAL, &logInterval[0], 2, 0);
	if (err_code != NRF_SUCCESS) return NRF_SUCCESS;

	// Application version
	char appVersion[] = APP_VERSION;
	err_code = tbs_char_add(p_tbs, BLE_UUID_TBS_CHAR_APPVERSION, &appVersion[0], 5, 0);

	return err_code;

}

// Function to be called when updating characteristic value
void tbs_txpower_characteristic_update(ble_tbs_t *p_our_service, int32_t *temperature_value)
{
    // OUR_JOB: Step 3.E, Update characteristic value

}
