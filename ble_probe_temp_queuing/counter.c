//const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(0); 												/**< Declaring an instance of nrf_drv_rtc for RTC0. */

/** @brief: Function for handling the RTC0 interrupts.
 * Triggered on TICK and COMPARE0 match.
 */
static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    if (int_type == NRF_DRV_RTC_INT_COMPARE0)
    {
        nrf_gpio_pin_toggle(BSP_BOARD_LED_0);
    }
    else if (int_type == NRF_DRV_RTC_INT_TICK)
    {
        nrf_gpio_pin_toggle(BSP_BOARD_LED_1);
    }
}

static void lfclk_config(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_clock_lfclk_request(NULL);
}


static void rtc_config(void)
{	
    uint32_t err_code;

    //Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = 4095;
		err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
    APP_ERROR_CHECK(err_code);
    //Enable tick event & interrupt
//    nrf_drv_rtc_tick_enable(&rtc,true);

    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
//    err_code = nrf_drv_rtc_cc_set(&rtc,0,COMPARE_COUNTERTIME*RTC0_CONFIG_FREQUENCY,true);
//    APP_ERROR_CHECK(err_code);

    //Power on RTC instance after clearing
		nrf_drv_rtc_counter_clear(&rtc);
		nrf_drv_rtc_enable(&rtc);
}

void counter_start(void)
{
    nrf_drv_rtc_counter_clear(&m_rtc);

    // Power on!
    nrf_drv_rtc_enable(&m_rtc);
}


void counter_stop(void)
{
    nrf_drv_rtc_disable(&m_rtc);
}


uint32_t counter_get(void)
{
    return(nrf_drv_rtc_counter_get(&m_rtc));
}

/** @}
 *  @endcond
 */
