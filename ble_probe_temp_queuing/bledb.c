#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include "nrf.h"
#include "fds.h"
#include "sdk_errors.h"
#include "SEGGER_RTT.h"
#include "bledb.h"
#include "app_util.h"
#include "nrf_delay.h"
#include "ble_nus.h"
#include "main.h"
#define  NRF_LOG_MODULE_NAME 			"BLEDB"
#include "nrf_log.h"

volatile uint8_t nus_tx_complete = 0;
volatile bool initFlag = false;
volatile bool gcDone = false;
volatile bool delRecordFlag = false;
volatile bool writeFlag = false;
volatile bool rolloverFlag = false;


uint32_t recCounter;

#ifdef CONCAT_NUS_DATA
/**Keeps track of number of telemetry data points in each record.
   Reset to 0 when it reaches CONCAT_RATIO or when device restarts (stored in RAM) */
uint16_t concatCounter = 0;
uint32_t concatDeltaT;
uint32_t concatLastTimeStamp;
uint16_t concatData[CONCAT_DATA_LEN];
uint16_t concatDataTemp[CONCAT_DATA_LEN];
#endif


void nus_tx_flag_set(void)
{
	nus_tx_complete = 1;
}

uint16_t get_recKey(void)
{
	return recCounter+REC_KEY_START;
}

void recCounter_init(uint16_t reckey_init)
{
	recCounter = reckey_init - REC_KEY_START;
}


void my_fds_evt_handler(fds_evt_t const * const p_fds_evt)
{
	uint32_t data[WORDLEN_DATAPACKET] = {0};
	uint8_t dataLen;
	uint16_t fileID = p_fds_evt->write.file_id;
	uint16_t recKey = p_fds_evt->write.record_key;
	uint16_t recID = p_fds_evt->write.record_id;

	//NRF_LOG_DEBUG("FDS Evt %d, result %d\r\n",p_fds_evt->id ,p_fds_evt->result );

switch (p_fds_evt->id)
{
	case FDS_EVT_INIT:
		if (p_fds_evt->result != FDS_SUCCESS)
		{
		  NRF_LOG_ERROR("FDS Initialization Failed %d\n",p_fds_evt->result);  // Initialization failed.
		}
		else  NRF_LOG_INFO("FDS Initialized\n");
		initFlag = true;
		break;
	case FDS_EVT_WRITE:
		if (p_fds_evt->result == FDS_SUCCESS)
		{
			NRF_LOG_INFO("Wrote file,rec,recID :%04x,%04x,%d  recCounter: %d, rolloverFlag: %d \r\n",
												fileID, recKey, recID, recCounter, rolloverFlag);

			if (fileID == FILE_ID) ++recCounter; 		// If telemetry data saved successfully, increment record key
			fds_read(fileID, recKey, data, &dataLen);
		}
		else
		{
			NRF_LOG_ERROR("FDS write failed for file,record: %04x,%04x result:%d\r\n", fileID, recKey,p_fds_evt->result);
		}
		writeFlag = true;
		break;
	case FDS_EVT_UPDATE:
		if (p_fds_evt->result == FDS_SUCCESS)
		{
			NRF_LOG_INFO("Updated file,record,recID: %04x,%04x,%d \r\n", fileID, recKey, recID);
			fds_read(fileID, recKey, data, &dataLen);
		}
		break;
	case FDS_EVT_DEL_FILE:
		if (p_fds_evt->result == FDS_SUCCESS)
		{
			fds_gc();
		}
		break;
	case FDS_EVT_DEL_RECORD:
		NRF_LOG_DEBUG("Deleted fileID,recKey:[%04x,%04x], result: %d \r\n",p_fds_evt->del.file_id,p_fds_evt->del.record_key, p_fds_evt->result);
		if (p_fds_evt->result == FDS_SUCCESS)
		{
			if (p_fds_evt->del.record_id % FREQ_OF_RUN_GC < 2)
			{
				NRF_LOG_INFO("Scheduled GC being run\r\n");
				fds_gc();
				//wait_for_fds_evt(FDS_EVT_GC);
			}
		}
		delRecordFlag = true;
		break;
	case FDS_EVT_GC:
		if (p_fds_evt->result == FDS_SUCCESS)
		{
			NRF_LOG_DEBUG("GC Done, space reclaimed %d\n", p_fds_evt->gc.space_reclaimed);
		}
		gcDone = true;
		break;
	default:
		break;
    }
}



ret_code_t fds_write(uint16_t fileID, uint16_t recKey, uint32_t data[], uint16_t dataLen)
{
	fds_record_t        record;
	fds_record_desc_t   record_desc;
	fds_record_chunk_t  record_chunk;

	// Set up data.
	static uint32_t dataPacket_test[3];
	for(uint8_t i = 0; i < dataLen; i++) {
	dataPacket_test[i] = data[i];
	}

	record_chunk.p_data         = &dataPacket_test[0];
	record_chunk.length_words   = dataLen;
	// NRF_LOG_DEBUG("Write dataIn: %08x, %08x, %08x, Size:%d\r\n", data[0],data[1],data[2],record_chunk.length_words);
	// Set up record.
	record.file_id              = fileID;
	record.key              	  = recKey;
	record.data.p_chunks        = &record_chunk;
	record.data.num_chunks      = 1;

	ret_code_t ret = fds_record_write(&record_desc, &record);

	if (ret != FDS_SUCCESS)
	{
			return ret;
	}
//		NRF_LOG_DEBUG("Wrote recordID:%d \r\n",record_desc.record_id);
	return NRF_SUCCESS;
}


ret_code_t save_lastseen(uint16_t fileID, uint16_t recKey)
{
	fds_flash_record_t  flash_record;
	fds_record_desc_t   record_desc;
	uint32_t 			data[WORDLEN_DATAPACKET] = {0};

	fds_find_token_t    ftok ={0};//Important, make sure you zero init the ftok token
	uint32_t err_code;
	uint32_t *dataTemp;


	// Loop until all records with the given key and file ID have been found.
	while (fds_record_find(fileID, recKey, &record_desc, &ftok) == FDS_SUCCESS)
	{
		err_code = fds_record_open(&record_desc, &flash_record);
		if ( err_code != FDS_SUCCESS)
		{
			NRF_LOG_ERROR("error opening record: %u\n", err_code);
			return err_code;
		}

		NRF_LOG_DEBUG("Data read back from file,rec,recID [%04x,%04x,%d] = ",
												fileID, recKey, record_desc.record_id);
		dataTemp = (uint32_t *) flash_record.p_data;

		for (uint8_t i=0;i<flash_record.p_header->tl.length_words;i++)
		{
			NRF_LOG_RAW_INFO("%08x ",dataTemp[i]);
			data[i] = dataTemp[i];
		}
		NRF_LOG_RAW_INFO("\r\n");
		
		err_code = fds_write(BACKUP_FILE_ID, REC_KEY_LASTSEEN, dataTemp, flash_record.p_header->tl.length_words);
		if (err_code != FDS_SUCCESS)
		{
			NRF_LOG_ERROR("Update error : %d",err_code);
			return err_code;
		}
	}
	return NRF_SUCCESS;
}


ret_code_t fds_read(uint16_t fileID, uint16_t recKey, uint32_t data[], uint8_t* dataLen)
{
	fds_flash_record_t  flash_record;
	fds_record_desc_t   record_desc;
	fds_find_token_t    ftok ={0};//Important, make sure you zero init the ftok token
	uint32_t err_code;
	uint32_t *dataTemp;

	// Loop until all records with the given key and file ID have been found.
	while (fds_record_find(fileID, recKey, &record_desc, &ftok) == FDS_SUCCESS)
	{
		err_code = fds_record_open(&record_desc, &flash_record);
		if ( err_code != FDS_SUCCESS)
		{
			NRF_LOG_ERROR("error opening record: %u\n", err_code);
			return err_code;
		}
		
		NRF_LOG_DEBUG("Data read back from file,rec,recID [%04x,%04x,%d] = ",
												fileID, recKey, record_desc.record_id);
		dataTemp = (uint32_t *) flash_record.p_data;
		for (uint8_t i=0;i<flash_record.p_header->tl.length_words;i++)
		{
			NRF_LOG_RAW_INFO("%08x ",dataTemp[i]);
			data[i] = dataTemp[i];
		}
		*dataLen = flash_record.p_header->tl.length_words;
		NRF_LOG_RAW_INFO("\r\n");
		// Access the record through the flash_record structure.
		// Close the record when done.
		err_code = fds_record_close(&record_desc);
		if (err_code != FDS_SUCCESS)
		{
			return err_code;
		}
		break;				// Stop at the first found record and return
	}
	return NRF_SUCCESS;
}


ret_code_t fds_find_and_delete (uint16_t fileID, uint16_t recKey)
{
	fds_record_desc_t   record_desc;
	fds_find_token_t    ftok;
	uint16_t 			count = 0;
	ftok.page	=	0;
	ftok.p_addr	=	NULL;
	
	//NRF_LOG_DEBUG("Deleting fileID,recKey [%04x,%04x]\r\n", fileID, recKey);

	// Loop and find records with same ID and rec key and mark them as deleted.
	while (fds_record_find(fileID, recKey, &record_desc, &ftok) == FDS_SUCCESS)
	{
		fds_record_delete(&record_desc);
		//wait_for_fds_evt(FDS_EVT_DEL_RECORD);
		count++;
	}
	if (count>0)	return NRF_SUCCESS;
	else return BLEDB_ERROR_NO_RECORD_FOUND;
}


static ret_code_t bledb_fds_init (void)
{
	recCounter = 0;
	ret_code_t ret = fds_register(my_fds_evt_handler);
	if (ret != FDS_SUCCESS)
	{
				return ret;

	}
	ret = fds_init();
	if (ret != FDS_SUCCESS)
	{
			return ret;
	}

	return NRF_SUCCESS;
		
}

ret_code_t fds_cleanup(uint32_t fileID)
{
	ret_code_t ret = fds_file_delete(fileID);
		if (ret == FDS_SUCCESS)
		{
			ret = fds_gc();
		}
		else
		{
			NRF_LOG_ERROR("Flash cleanup error:%d",ret);
		}
	return ret;
}

#ifdef CONCAT_NUS_DATA

// Function to concat 3 data points into a single 20byte packet.
// Returns true if the packet is ready to be sent through NUS
static bool concat_nus_datapacket(uint32_t * data, uint16_t dataLen)
{
	// Start from previously left residue (or all 1s)
	memcpy(concatData,concatDataTemp,sizeof(concatData));

	uint32_t concatDeltaTnew = data[1] - concatLastTimeStamp;
	uint16_t timeStamp_temp[2] 	= {(uint16_t)data[1],(uint16_t)(data[1]>>16)};
	uint16_t data_temp[2]	 	= {(uint16_t)data[2],(uint16_t)(data[2]>>16)};
	concatLastTimeStamp = data[1];

	// Depending on concatenation stage, assign data and deltaT values
	switch(concatCounter)
	{
		case 0:				// never record deltaT on first concatenated record; Update D0
			concatData[0]	= (uint16_t)data[0];			// record key (RK0)
			concatData[1]	= timeStamp_temp[0];			// time stamp LSB (Ts0)
			concatData[2]	= timeStamp_temp[1];			// time stamp MSB (Ts0)
			concatData[3]	= 0;							// deltaT
			concatData[4+2*concatCounter] = data_temp[0];	// D0 Humid+Batt
			concatData[5+2*concatCounter] = data_temp[1];	// D0 Temperature
			concatCounter++;
			memcpy(concatDataTemp,concatData,sizeof(concatData));	// Save residue
			return false;									// Don't send
			break;
		case 1:
			concatDeltaT = concatDeltaTnew;
			if (data[0]|data[1]|data[2])	{			// If data isn't from an unwritten flash record
				if ((concatDeltaT < 0x0000FFFF) || (concatData[0] != 0))
				// Add to current packet and increment counter
				// Residue made same as current packet to continue concatenation
				{
					concatData[0]	= (uint16_t)data[0];			// record key (RK1)
					concatData[1]	= timeStamp_temp[0];			// time stamp LSB (Ts1)
					concatData[2]	= timeStamp_temp[1];			// time stamp MSB (Ts1)
					concatData[3] 	= (uint16_t)concatDeltaT;
					concatData[4+2*concatCounter] = data_temp[0];
					concatData[5+2*concatCounter] = data_temp[1];
					concatCounter++;
					memcpy(concatDataTemp,concatData,sizeof(concatData));	// Save residue
					return false;
				}
				else	{
				// Send without adding current rec to packet
				// Save current packet as RK0,TS0,D0.. to residue
				// For next packet, start from counter=1 and use residue as D0
					memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
					concatDataTemp[0]	= (uint16_t)data[0];
					concatDataTemp[1]	= timeStamp_temp[0];
					concatDataTemp[2]	= timeStamp_temp[1];
					concatDataTemp[3] 	= 0;
					concatDataTemp[4]	= data_temp[0];
					concatDataTemp[5]	= data_temp[1];
					return true;										// ready to send
				}
			}
			else	{
				// Add to current packet and increment counter
				// Residue made same as current packet to continue concatenation
				if (concatData[0] == 0)	{
					concatData[4+2*concatCounter] = data_temp[0];
					concatData[5+2*concatCounter] = data_temp[1];
					concatCounter++;
					memcpy(concatDataTemp,concatData,sizeof(concatData));	// Save residue
					return false;											// dont send packet yet
				}
				else {
					// Send without adding current rec to packet
					// Save current packet as RK0,TS0,D0.. to residue
					// For next packet, start from counter=1 and use residue as D0
					memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
					concatDataTemp[0]	= (uint16_t)data[0];
					concatDataTemp[1]	= timeStamp_temp[0];
					concatDataTemp[2]	= timeStamp_temp[1];
					concatDataTemp[3] 	= 0;
					concatDataTemp[4] 	= data_temp[0];
					concatDataTemp[5] 	= data_temp[1];
					return true;											// ready to send
				}
			}
			break;
		case CONCAT_RATIO-1:						// Always return true (send data) from this step
		if (data[0]|data[1]|data[2])	{			// If data isn't from unwritten flash record
			if ((abs(concatDeltaTnew - concatDeltaT) < CONCAT_TIMESTAMP_ERROR_MARGIN) // if deltaT is same as before
					&&	(concatData[0] !=0))
				// Add to current packet
				// Residue reset to all 1s and reset counter to 0
			{
				concatData[0]	= (uint16_t)data[0];			// record key (RK2)
				concatData[1]	= timeStamp_temp[0];			// time stamp LSB (Ts2)
				concatData[2]	= timeStamp_temp[1];			// time stamp MSB (Ts2)
				concatData[3]	= (uint16_t)concatDeltaT;
				concatData[4+2*concatCounter] = data_temp[0];
				concatData[5+2*concatCounter] = data_temp[1];
				memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
				concatCounter = 0;
				NRF_LOG_DEBUG("deltaTold = %d, deltaTNew = %d\r\n", concatDeltaT, concatDeltaTnew);
			}
			else	{
				// Send without adding current rec to packet. But make D2 all 0s
				// Save current packet as RK0,TS0,D0.. to residue
				// For next packet, start from counter=1 and use residue as D0
				//concatData[4+2*concatCounter] = 0;
				//concatData[5+2*concatCounter] = 0;
				memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
				concatDataTemp[0]	= (uint16_t)data[0];
				concatDataTemp[1]	= timeStamp_temp[0];
				concatDataTemp[2]	= timeStamp_temp[1];
				concatDataTemp[3] 	= 0;
				concatDataTemp[4]	= data_temp[0];
				concatDataTemp[5]	= data_temp[1];
				concatCounter = 1;
			}
		}
		else
		{
			if (concatData[0] == 0){
				// Add to current packet
				// Residue rest to all 1s and reset counter
				concatData[4+2*concatCounter] = data_temp[0];
				concatData[5+2*concatCounter] = data_temp[1];
				concatCounter = 0;
				memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
			}
			else {
				// Send without adding current rec to packet
				// Save current packet as RK0,TS0,D0.. to residue
				// For next packet, start from counter=1 and use residue as D0
				memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
				concatDataTemp[0]	= (uint16_t)data[0];
				concatDataTemp[1]	= timeStamp_temp[0];
				concatDataTemp[2]	= timeStamp_temp[1];
				concatDataTemp[3] 	= 0;
				concatDataTemp[4]	= data_temp[0];
				concatDataTemp[5]	= data_temp[1];
				concatCounter = 1;
			}
		}
		return true;									// send packet at this stage always

		default:
			concatCounter = CONCAT_RATIO-1;							// catch all: send data and start a fresh packet
			memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));	// Reset residue
			return false;
	}
}
static void nus_concatData_send(ble_nus_t * p_nus){
	uint8_t *p_dataPacket = (uint8_t *)concatData;
	uint32_t ret = ble_nus_string_send(p_nus, p_dataPacket, CONCAT_DATA_LEN*2);
	if (ret == FDS_SUCCESS) {
		for (uint8_t i=0;i<CONCAT_DATA_LEN*2;i++) {
			NRF_LOG_RAW_INFO("%02x",*p_dataPacket++);
		}
		NRF_LOG_RAW_INFO("\r\n");
	}
	else NRF_LOG_ERROR("nus_data_send error: %d",ret);

}

#endif

ret_code_t bledb_dataToDB (uint16_t fileID, uint16_t recKey, uint32_t * data, uint16_t dataLen)
{
	ret_code_t ret = FDS_SUCCESS;
	recKey = (recCounter % DATA_POINTS) + REC_KEY_START;

	if (rolloverFlag)
	{
		NRF_LOG_DEBUG("Old record will be replaced in DB\r\n");
		ret = fds_find_and_delete(fileID, recKey);
		if (ret != FDS_SUCCESS) NRF_LOG_ERROR("Find and delete failed for fileID,recKey [%d,%d]\r\n",fileID,recKey);
	}

	// write is made blocking to ensure that the following REC_KEY_ROLLOVERSTATE update happens cleanly
	// otherwise the data written to REC_KEY_INITSTATE may get corrupted (seen in v1.0.3)
	ret = fds_write(fileID, recKey, data, dataLen);
	wait_for_fds_evt(FDS_EVT_WRITE);
	if (ret != FDS_SUCCESS)
	{
		switch (ret)
		{
			case FDS_ERR_NO_SPACE_IN_FLASH:
				NRF_LOG_WARNING("No space in flash, running GC\r\n");
				//ret = fds_cleanup(fileID);
				fds_gc();
				wait_for_fds_evt(FDS_EVT_GC);
				ret = fds_write(fileID, recKey, data, dataLen);
				wait_for_fds_evt(FDS_EVT_WRITE);
				return ret;
			default:
				return ret;
		}
	}

	// Update the REC_KEY_ROLLOVERSTATE if rollover has happened.
	// code pushed to after the data logging to make sure data is not corrupted as seen in bug in v1.0.3
	if (!rolloverFlag)
	{
		NRF_LOG_DEBUG("New record will be written\r\n");
		if (recCounter >= DATA_POINTS)
		{
			// Set rolloverFlag and save it in persistent memory too
			// data in record may be corrupt because value rollOverState[0] is not being maintained properly
			// hence any non-zero value in REC_KEY_ROLLOVERSTATE should denote reckey rollover
			const uint32_t rollOverState[1] = {0x00000001};
			ret = fds_find_and_delete(BACKUP_FILE_ID, REC_KEY_ROLLOVERSTATE);
			ret = fds_write(BACKUP_FILE_ID, REC_KEY_ROLLOVERSTATE, &rollOverState[0], 1);
			if (ret == FDS_SUCCESS)	rolloverFlag = true;
		}
	}
	return ret;
}

sync_type_t check_startRecKey(uint16_t inRecKey, uint16_t currentRecKey)
{
	nusRecKey = inRecKey;
	if (inRecKey == (uint16_t)FDS_RECORD_KEY_DIRTY)	return SYNCTYPE_INVALID;

	if (inRecKey <= currentRecKey)
	{
		if (inRecKey < currentRecKey - DATA_POINTS)  nusRecKey = currentRecKey - DATA_POINTS;
		return SYNCTYPE_STRAIGHT;
	}
	else
	{
		if (!rolloverFlag)
		{
			nusRecKey = REC_KEY_START;
			return SYNCTYPE_STRAIGHT;
		}
		if (currentRecKey > DATA_POINTS)
		{
			nusRecKey = currentRecKey - DATA_POINTS;
			return SYNCTYPE_STRAIGHT;
		}
		else if ((uint32_t)inRecKey < currentRecKey + 0xFFFF - DATA_POINTS)
		{
			nusRecKey = currentRecKey + 0xFFFF - DATA_POINTS;
			return SYNCTYPE_ROLLOVER;
		}
		else return SYNCTYPE_ROLLOVER;
	}
}


uint8_t check_nusRecKey(uint16_t inRecKey, uint16_t currentRecKey, sync_type_t syncType)
{
	switch(syncType)
	{
		case SYNCTYPE_STRAIGHT:
			if (nusRecKey < currentRecKey) return NUS_CONTINUE;
			else if(nusRecKey == currentRecKey) return NUS_STOP;
				else return NUS_NOACTION;
		case SYNCTYPE_ROLLOVER:
			if ((nusRecKey > currentRecKey) && (nusRecKey < 0xFFFF-DATA_POINTS+currentRecKey)) return NUS_NOACTION;
			else if (nusRecKey == currentRecKey) return NUS_STOP;
				else return NUS_CONTINUE;
		case SYNCTYPE_INVALID:
			return NUS_NOACTION;
		default:
			NRF_LOG_WARNING("Invalid SYNCTYPE at check_nusRecKey call ");
			return NUS_NOACTION;
	}
}




ret_code_t nus_dataPacket_send(ble_nus_t * p_nus, uint32_t data[], uint8_t dataLen)
{
	uint32_t ret = NRF_SUCCESS;

#ifdef CONCAT_NUS_DATA

	bool sendData = false;
	sendData = concat_nus_datapacket(data, dataLen);
	if (sendData){
		nus_concatData_send(p_nus);
	}
	else sync_handler();


#else

	uint8_t *p_dataPacket = (uint8_t *)data;
	uint8_t dataLengthInBytes = WORDLEN_DATAPACKET*4;
	uint8_t dataByteArray[dataLengthInBytes];

	for (uint8_t i=0;i<dataLengthInBytes;i++)
	{
		dataByteArray[i] = *(p_dataPacket+i);
		//NRF_LOG_RAW_INFO("%02x",dataByteArray[i]);
	}

	ret = ble_nus_string_send(p_nus, p_dataPacket, dataLengthInBytes);
	if (ret != NRF_SUCCESS)
	{
		NRF_LOG_ERROR("NUS string send error: %d",ret);
		return ret;
	}

	NRF_LOG_INFO("Data sent over NUS:");
	for (uint8_t i=0;i<dataLengthInBytes;i++)
	{
		NRF_LOG_RAW_INFO("%02x",dataByteArray[i]);
	}
#endif

	return ret;
}

ret_code_t nus_eom_send(ble_nus_t * p_nus, uint8_t eomType)
{
	uint32_t ret = NRF_SUCCESS;
#ifdef CONCAT_NUS_DATA
	uint32_t data[] = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};
	uint8_t dataLengthInBytes = CONCAT_DATA_LEN*2;

	// Send remaining concatenated data before EOM
	if (concatCounter != 0) nus_concatData_send(p_nus);

	// Reset residue and counter for next sync
	concatCounter = 0;
	memset(&concatDataTemp,0xFFFF,sizeof(concatDataTemp));

#else
	uint32_t data[] = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};
	uint8_t dataLengthInBytes = WORDLEN_DATAPACKET*4;
#endif

	uint8_t *p_dataPacket = (uint8_t *)data;

	uint8_t dataByteArray[dataLengthInBytes];
	for (uint8_t i=0;i<dataLengthInBytes;i++)
	{
		dataByteArray[i] = p_dataPacket[i];
		//NRF_LOG_RAW_INFO("%02x",dataByteArray[i]);
	}

	ret = ble_nus_string_send(p_nus, p_dataPacket, dataLengthInBytes);
	if (ret != NRF_SUCCESS)
	{
		NRF_LOG_ERROR("NUS string send error: %d",ret);
		return ret;
	}
	else {
		NRF_LOG_INFO("Data sent over NUS:");
		for (uint8_t i=0;i<dataLengthInBytes;i++)
		{
			NRF_LOG_RAW_INFO("%02x",dataByteArray[i]);
		}
	}
	return ret;
}


ret_code_t payload_to_central_async (ble_nus_t * p_nus, uint16_t nusRecKey)
{
	uint32_t recKey ;
	uint32_t data[WORDLEN_DATAPACKET] = {0};
	uint8_t dataLen;
	ret_code_t ret = FDS_SUCCESS;
	// Map inbound nusRecKey to a record in the DB
	if (nusRecKey != FDS_RECORD_KEY_DIRTY)
	{
		recKey = ((nusRecKey-REC_KEY_START) % DATA_POINTS) + REC_KEY_START;
		ret = fds_read(FILE_ID, recKey, data, &dataLen);
	}

	//	Handle exceptions like flash full etc : FDS_ERR_*
	if (ret != FDS_SUCCESS)
	{
		NRF_LOG_ERROR("Record read error: %d", ret);
		switch (ret)
		{
			case FDS_ERR_OPERATION_TIMEOUT:
				NRF_LOG_WARNING("RECKEY not found in DB, err:%d\r\n", ret);
				break;
			case FDS_ERR_RECORD_TOO_LARGE:
				NRF_LOG_WARNING("RECKEY too large, err:%d\r\n", ret);
				break;
			default:
				ret = FDS_ERR_INTERNAL;
				break;
		}
	}
	else	{
		ret = nus_dataPacket_send(p_nus, &data[0], dataLen);
	}
	return ret;
}

void wait_for_fds_evt(fds_evt_id_t id)
{
	switch (id)
    {
        case FDS_EVT_INIT:
            while(!initFlag) __WFE;
				initFlag = false;
            break;

        case FDS_EVT_WRITE:
            while(!writeFlag) __WFE;
				writeFlag = false;
            break;

		case FDS_EVT_DEL_RECORD:
            while(!delRecordFlag) __WFE;
            	delRecordFlag = false;
            break;

		case FDS_EVT_GC:
            while(!gcDone) __WFE;
				gcDone = false;
            break;
						
        default:
            break;				
		}
}


void bledb_fds_setup(void)
{

	uint32_t data[WORDLEN_DATAPACKET] 	= {0};
	uint8_t dataLen 	= sizeof(data);
	uint32_t err_code 	= NRF_SUCCESS;

	// Wait for fds to be initialized before any read/write
	err_code =bledb_fds_init();
	APP_ERROR_CHECK(err_code);
	// POLL FOR INIT CALLBACK
	wait_for_fds_evt(FDS_EVT_INIT);

	err_code = fds_read(BACKUP_FILE_ID, REC_KEY_INITSTATE, data, &dataLen);
	if (err_code != NRF_SUCCESS) NRF_LOG_ERROR("FDS read error %d", err_code);

	// Erase files and setup local db if first time
	if ((data[0]&0x00000001) == 0)
	{
		NRF_LOG_INFO("\r\n\n----Device Flash Init Begin----\n");

		err_code = fds_file_delete(FILE_ID);
		if (err_code != FDS_SUCCESS) NRF_LOG_ERROR("file del err: %d \n",err_code);
		APP_ERROR_CHECK(err_code);
		NRF_LOG_RAW_INFO("\r\n\n");
		// Wait for GC to complete on FILE
		wait_for_fds_evt(FDS_EVT_GC);

		// Uncomment after all tags with db bug fix are dfu-ed
/*
		err_code = fds_file_delete(BACKUP_FILE_ID);
		if (err_code != FDS_SUCCESS) NRF_LOG_ERROR("file del err: %d \n",err_code);
		APP_ERROR_CHECK(err_code);
		NRF_LOG_RAW_INFO("\r\n\n");
		// Wait for GC to complete on FILE
		wait_for_fds_evt(FDS_EVT_GC);
*/

		NRF_LOG_INFO("Writing data to LASTSEEN record:\r\n");
		uint32_t testData[] = {(uint32_t)REC_KEY_START,0x00000000,0x00000000};
		err_code = fds_write(BACKUP_FILE_ID, REC_KEY_LASTSEEN, testData, 3);
		APP_ERROR_CHECK(err_code);
		// Wait for write done event
		wait_for_fds_evt(FDS_EVT_WRITE);

		// Write 1 to RecKey_initstate to keep record of whether flash has been initialized
		NRF_LOG_INFO("Writing data to INITSTATE record:\r\n");
		uint32_t setInitState[1] = {0x00000001};
		err_code = fds_write(BACKUP_FILE_ID, REC_KEY_INITSTATE, &setInitState[0], 1);
		APP_ERROR_CHECK(err_code);
		// Wait for write done event
		wait_for_fds_evt(FDS_EVT_WRITE);

		// Write 0 to RecKey_rolloverstate to keep record of whether flash has been initialized
		NRF_LOG_INFO("Resetting ROLLOVERSTATE record:\r\n");
		uint32_t setRolloverState[1] = {0x00000000};
		err_code = fds_write(BACKUP_FILE_ID, REC_KEY_ROLLOVERSTATE, &setRolloverState[0], 1);
		APP_ERROR_CHECK(err_code);
		// Wait for write done event
		wait_for_fds_evt(FDS_EVT_WRITE);


		NRF_LOG_RAW_INFO("----Device Flash Init Done----\r\n\n\n");
		nrf_delay_ms(1000);
	}

	// Set rollover flag, i.e. reckey has gone beyond DATA_POINTS; read from REC_KEY_ROLLOVERSTATE
	err_code = fds_read(BACKUP_FILE_ID, REC_KEY_ROLLOVERSTATE, data, &dataLen);
	if (err_code != NRF_SUCCESS) NRF_LOG_ERROR("FDS read error %d", err_code);

	if (data[0]) rolloverFlag = true;
	else rolloverFlag = false;
}
