:: Input arguments (only one at a time)
:: GEN_PRIV_KEY
:: GEN_PUB_KEY
:: PUSH_TO_PHONE <zip_name_postfix>
:: FLASH_DEVICE

@echo off

set soft_device=..\..\..\..\components\softdevice\s132\hex\s132_nrf52_3.0.0_softdevice.hex
set bootloader=..\..\..\queueing-dfu\bootloader_secure\pca10040\armgcc\_build\nrf52832_xxaa_s132.hex
set app_build_path=..\pca10040\s132\armgcc\_build\
set app=%app_build_path%nrf52832_xxaa.hex
set priv_key=priv.pem
set public_key=..\..\..\queueing-dfu\bootloader_secure\dfu_public_key.c
set app_version=0x00
set boot_version=0x00
set boot_settings=bootloader_setting.hex
set merged_boot=bootloader_with_appsettings.hex
set merged_hex=sd_bl_app_merged.hex
set nRF_family=nrf52


:: build application 
call :build_app

:: Create Bootloader settings and a merged bootloader hex
if exist %merged_boot% goto gen_priv_key
	nrfutil settings generate --family NRF52 --application %app% --application-version %app_version% --bootloader-version %boot_version% --bl-settings-version 1 %boot_settings%
	if %errorlevel% neq 0 goto error_handler 
	echo Bootloader Settings generated
	mergehex --merge %bootloader% %boot_settings% --output %merged_boot% 
	if %errorlevel% neq 0 goto error_handler
	echo Bootloader with App settings generated
	
	
:gen_priv_key
:: If keys need to be generated 
if not $%1$ == $GEN_PRIV_KEY$ goto gen_pub_key
	nrfutil keys generate priv.pem
	if %errorlevel% neq 0 goto error_handler
	echo Generated private key

	
:gen_pub_key
if not $%1$ == $GEN_PUB_KEY$ goto create_zip
	nrfutil keys display --key pk --format code %priv_key% --out_file public_key.c
	if %errorlevel% neq 0 goto error_handler
	echo Generated public key
	echo Now copying to bootloader folder
	copy public_key.c %public_key%  

	
:create_zip
if not $%1$ == $PUSH_TO_PHONE$ goto flash_device 
	:: Generate zip of just the application
	set out_zip=temp_queuing_dfu_app%2.zip

	nrfutil pkg generate --application  %app% --application-version %app_version% --application-version-string 1.0.2 --hw-version 52 --sd-req 0x8C,0x91 --key-file %priv_key% %out_zip%
	if %errorlevel% neq 0 goto error_handler
	
	adb wait-for-device push %out_zip% /sdcard/Download/


:flash_device
if not $%1$ == $FLASH_DEVICE$ goto EoF
	mergehex --merge %soft_device% %app% %merged_boot% --output %merged_hex%
	if %errorlevel% neq 0 goto error_handler

	nrfjprog --recover -f %nRF_family%	
	nrfjprog  --program %merged_hex% --chiperase -f %nRF_family% --verify --reset
	if %errorlevel% neq 0 goto error_handler

	nrfjprog --rbp ALL -f %nRF_family%
	if %errorlevel% neq 40 ( 
	echo ERROR encountered ... Exiting
	exit /b %errorlevel% ) else echo IGNORE PREVIOUS ERROR

	nrfjprog --reset -f %nRF_family%
	nrfjprog --memrd 0x0000C003 -f %nRF_family%
	if %errorlevel% neq 16 ( 
	echo Readback not protected. Please check with developer ... Exiting
	exit /b %errorlevel% ) else echo IGNORE PREVIOUS ERROR. Readback protection has been set



:: End of Files

:EoF
echo %1 operation successfully completed
exit /b 0

:error_handler
echo Exiting at ERROR. Please rerun script
exit /b 1


:: Functions' definitions

:build_app 
cd %app_build_path%..
make || goto error_handler
cd ..\..\..\DFU\
exit /b 0
