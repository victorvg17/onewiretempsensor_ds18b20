Advertised Packet : 
<Data>      <len>    <type>    <value>
Flags      :    2     0x01      0x04 or 0x05 (depending on whether tag is connectable or not)
SerData    :   15     0x16      Ser Data
Name       :    7     0x08      TagName(6 bytes)

Service Data : 
ServiceUUID (0xAB04, for temp) (0xAB05 for acceleration)
Company Id  (0x128B)
uint16_t dataPacket = {APP_COMPANY_IDENTIFIER, recKey,
                        timeStamp[0], timeStamp[1],
			             temp,  			 humid}
						 	
	
		
Uart Data: Each packet of length 20bytes
<RecKey> 	<TimeStamp>		<DeltaT>	<Data0>		<Data1>		<Data2>
2bytes		4bytes			2bytes		4bytes		4bytes		4bytes

Temperature Data format (UUID 0xAB04):	<Temp>		<Humidity>	<Battery>
										2byte sint	1byte uint	1byte uint

Acceleration Data format (UUID 0xAB05):	<AccX>		<AccY>		<AccZ>		<Battery>
										1byte sint	1byte sint	1byte uint	1byte uint
										
	

	
For flashing DFU code, cd to DFU folder and
	- dfu_init_flash.bat FLASH_DEVICE 			:: flashes bootloader to nrf device
	- dfu_init_flash.bat PUSH_TO_PHONE			:: copies DFU application zip to connected phone's /sdcard/Download/ path; 
												   Second argument can be provided to append to the zip name eg _debug
